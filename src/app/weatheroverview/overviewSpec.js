/* 
    Test for overview controller. Note that's just an ES6 component so nothing angular specific is used here!
*/

import {assert} from 'chai';
import sinon from 'sinon';

import OverviewCtrl from './overviewCtrl'; //sut

describe('when constructing the overview controller', function () {

    let overviewCtrl, apiPromise, applySpy;

    beforeEach(function () {
        let myApiMock = {
            fetchCitiesData: () => {
                apiPromise = Promise.resolve([{
                    name: 'Duckstad'
                }, {
                    name: 'Teststad'
                }]);
                return apiPromise;
            }
        };

        let scopeMock = {
            $apply: ()=>{}
        }

        applySpy = sinon.spy(scopeMock, "$apply");

        overviewCtrl = new OverviewCtrl(
            myApiMock, 
            {}, // ngnotify
           scopeMock
        );
    });

    it('the cities provided by the apiservice are set on controller and a call on scope.apply is done', function (done) {
        apiPromise.then(() => {
            assert.equal(overviewCtrl.cities[0].name, "Duckstad");
            assert.isTrue(applySpy.called);
            done();
        });
    });
});