'use strict';

/* 
    Keeping controllers light. Basicly only responsible for passing the data between service and the weather forecast components. 
*/
export default class OverviewCtrl {

    constructor(weatherApiService, ngNotify, $scope) {
        this.weatherApiService = weatherApiService;
        this.getWeatherOverview();
        this.ngNotify = ngNotify;
        this.$scope = $scope;
    }

    getWeatherOverview() {
        this.weatherApiService.fetchCitiesData().then(
            (cities) => {
                this.cities = cities;
                this.$scope.$apply(); // $apply nescessary because I wanted to use es6 promises instead of $q for this test project
            },
            (reject) => {
                this.ngNotify().set("Oh dear, error while fetching weather notify", {
                    type: 'error'
                });
                this.$scope.$apply(); // $apply nescessary because I wanted to use es6 promises instead of $q for this test project
            });
    }
}

OverviewCtrl.$inject = ['WeatherApiService', 'ngNotify', '$scope'];