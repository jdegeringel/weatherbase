/* 
    Test for forecast controller. Note that's just an ES6 component so nothing angular specific is used here!
*/

import {assert} from 'chai';
import sinon from 'sinon';

import ForecastCtrl from './forecastCtrl'; //sut

describe('when constructing the forecast controller', function () {

    let forecastCtrl, apiPromise, applySpy;

    beforeEach(function () {
        let myApiMock = {
            fetchCityForeCast: () => {
                apiPromise = Promise.resolve({
                    name: 'Duckstad'
                });
                return apiPromise;
            }
        };

        let scopeMock = {
            $apply: ()=>{}
        }

        applySpy = sinon.spy(scopeMock, "$apply");

        forecastCtrl = new ForecastCtrl(
            myApiMock, 
            {}, // routeparams
            {}, // ngnotify
           scopeMock
        );
    });

    it('the forecast provided by the apiservice are set on controller and a call on scope.apply is done', function (done) {
        apiPromise.then(() => {
            assert.equal(forecastCtrl.forecast.name, "Duckstad");
            assert.isTrue(applySpy.called);
            done();
        });
    });
});