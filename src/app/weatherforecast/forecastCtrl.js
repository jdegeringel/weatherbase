'use strict';

/* 
    Keeping controllers light. Basicly only responsible for passing the data between service and the weather forecast components. 
*/
export default class ForecastCtrl {

    constructor(weatherApiService, $routeParams, ngNotify, $scope) {
        this.weatherApiService = weatherApiService;
        this.getWeatherForecast($routeParams.city);
        this.ngNotify = ngNotify;
        this.$scope = $scope;
    }

    getWeatherForecast(cityToForecast) {
        this.weatherApiService.fetchCityForeCast(cityToForecast).then(
            (forecast) => {
                this.forecast = forecast;
                this.$scope.$apply();
            },
            (reject) => {
                this.ngNotify().set("Oh dear, error while fetching forecast", {type: 'error'});
                this.$scope.$apply();
            });
    }

}

ForecastCtrl.$inject = ['WeatherApiService', '$routeParams', 'ngNotify', '$scope'];