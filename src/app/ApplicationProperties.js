'use strict';

export default class ApplicationProperties {

    static getMyCities() {
         /* Here we define which cities to get, you can change this to other valid city id's and everything will be fine. */
        return  [
            {'id' : 2643743, displayName: 'LONDON', displayColor: '#2196F3'}, // LONDON
            {'id' : 524901, displayName: 'MOSCOW', displayColor: '#F44336'}, // MOSCOW
            {'id' : 3117735, displayName: 'MADRID', displayColor: '#009688'}, // MADRID
            {'id' : 2759633, displayName: 'ASSEN', displayColor: '#FF5722'}, // ASSEN
            {'id' : 587084, displayName: 'BAKU', displayColor: '#607D8B'} // BAKU
        ];


    } 

  

}
