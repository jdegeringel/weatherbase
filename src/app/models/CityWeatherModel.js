'use strict';


export default class CityWeatherModel {

    constructor(name, id, temp, icon, windDeg, windSpeed, time, displayColor) {
       this.name = name;
       this.id = id;
       this.temp = temp;
       this.icon = icon;
       this.windDeg = windDeg;
       this.windSpeed = windSpeed;
       this.time = time;
       this.displayColor = displayColor;
    }

  

}
