'use strict';

export default class ForecastModel {

    constructor(name, id, displayColor) {
        this.name = name;
        this.id = id;
        this.displayColor = displayColor;
        this.forecasts = [];
    }

    addForecast(temp, icon, windDeg, windSpeed, time) {
        this.forecasts.push({
            temp: temp,
            icon: icon,
            windDeg: windDeg,
            windSpeed: windSpeed,
            time: time
        });
    }



}