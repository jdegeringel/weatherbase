import  WeatherApiService from './services/weatherApiService';

import  OverviewCtrl from './weatheroverview/overviewCtrl.js';
import  ForecastCtrl from './weatherforecast/forecastCtrl.js';

import {cityOverviewComponent} from './components/cityOverviewComponent';
import {forecastComponent} from './components/forecastComponent';

/** 
  The forecast and overview component could be modulised even further (own angular module).
*/
var app = angular.module('weatherBase', [ 'ngRoute', 'ngNotify']);

app
    .service('WeatherApiService', WeatherApiService)
    .controller("OverviewCtrl", OverviewCtrl)
    .controller("ForecastCtrl", ForecastCtrl)
    .component("cityOverviewComponent", cityOverviewComponent)
    .component("forecastComponent", forecastComponent);


angular
    .module('weatherBase')
    .config(function($routeProvider){
        $routeProvider.when('/', {
            templateUrl : './app/weatheroverview/overview.html',
            controller : 'OverviewCtrl as vm'
        }).when('/forecast/:city', {
            templateUrl : './app/weatherforecast/forecast.html',
            controller : 'ForecastCtrl as vm'
        }).otherwise('/');
    });