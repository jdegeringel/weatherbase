/* 
    Test for API service. Note that's just an ES6 component so nothing angular specific is used here!
*/

import {
    assert
} from 'chai';
import sinon from 'sinon';

import WeatherApiService from './weatherApiService'; //sut

describe('when calling weather api service', function () {

    let weatherApiService;

    describe("on a succesfull weather overview call", function () {
        beforeEach(function () {

            //mock http serivce that returns 2 cities as mock data
            let myHttp = () => {
                return Promise.resolve({
                    data: {
                        list: [{
                            name: 'Duckstad',
                            id: 2643743,
                            weather: [{
                                icon: '01d'
                            }],
                            main: {
                                temp: 24
                            },
                            wind: {}
                        }]
                    }
                });
            };
            weatherApiService = new WeatherApiService(myHttp);
        });
        it('a list of CityWeatherModels will be returned', function (done) {

            weatherApiService.fetchCitiesData().then((response) => {

                assert.equal(response[0].name, 'Duckstad');
                assert.equal(response[0].id, '2643743');
                assert.equal(response[0].displayColor, '#2196F3');
                assert.equal(response[0].icon, '01d');
                assert.equal(response[0].temp, 24);

                done();

            });
        });
    });


    describe("on a error on a  weather overview call", function () {
        beforeEach(function () {

            let myHttp = () => {
                return Promise.reject("reject reason");
            };
            weatherApiService = new WeatherApiService(myHttp);
        });
        it('the service call will be rejected with resulting reject object', function (done) {

            weatherApiService.fetchCitiesData().then(
                (succes) => {}, //done never called so test will fail
                (reject) => {
                    assert.equal(reject, 'reject reason');
                    done();
                });
        });
    });

    describe("on a succesfull weather forecast call", function () {
        beforeEach(function () {

            //mock http serivce that returns 2 cities as mock data
            let myHttp = () => {
                return Promise.resolve({
                    data: {
                        city: {
                            id: 2643743
                        },
                        list: [{
                            weather: [{
                                icon: '01d'
                            }],
                            main: {
                                temp: 24
                            },
                            wind: {}
                        }]
                    }
                });
            };
            weatherApiService = new WeatherApiService(myHttp);
        });
        it('a ForecastModel will be returned with a list of forecast for requested city', function (done) {

            weatherApiService.fetchCityForeCast("Duckstad").then((response) => {

                assert.equal(response.name, 'Duckstad');
                assert.equal(response.id, '2643743');
                assert.equal(response.displayColor, '#2196F3');
                assert.equal(response.forecasts[0].icon, '01d');
                assert.equal(response.forecasts[0].temp, 24);

                done();

            });
        });
    });

    describe("on a error on a  weather forecast call", function () {
        beforeEach(function () {

            let myHttp = () => {
                return Promise.reject("reject reason");
            };
            weatherApiService = new WeatherApiService(myHttp);
        });
       it('the service call will be rejected with resulting reject object', function (done) {

            weatherApiService.fetchCityForeCast("Duckstad").then(
                (succes) => {}, //done never called so test will fail
                (reject) => {
                    assert.equal(reject, 'reject reason');
                    done();
                });
        });
    });
});