'use strict';

// no need to bind these classes to angular context o.a., just plain ES6 classes these 3
import ApplicationProperties from './../ApplicationProperties';
import CityWeatherModel from './../models/CityWeatherModel.js';
import ForecastModel from './../models/ForecastModel.js';

/**
 * Responsible for fetching the data from the openweathermap API. Returns a list of CityWeatherModels or a ForecastModel objects.
 * 
 * Interacts with ApplicationProperties to determine which cities to fetch.
 */
export default class WeatherApiService {

    constructor($http) {
        this.$http = $http;
        this.citiesToFetch = ApplicationProperties.getMyCities();

    }

    /** Returns the current weather overview per city as a list of CityWeatherModels.   */
    fetchCitiesData() {
      
        return new Promise((resolve, reject) => {
            this.$http({
                method: 'GET',
                url: 'http://api.openweathermap.org/data/2.5/group?id=' + this.makeCommaSeperatedCitiesId() + '&appid=9ae7cc8fabc82556be550290ccb8f1e2&units=metric'
            }).then((response) => {
                
                let cities = [];

                /* Loop trough every city from response. Will need to do more defensive checking in real life. */
                response.data.list.forEach((city) => {
                    cities.push(new CityWeatherModel(
                        city.name,
                        city.id,
                        city.main.temp,
                        city.weather[0].icon,
                        city.wind.deg,
                        city.wind.speed,
                        new Date(city.dt * 1000),
                        this.getColorFromProperties(city.id)
                    ));

                });

                resolve(cities);
            }, function errorCallback(response) {
                reject(response);
            });

        });
    }


    /** Returns a ForecastModel of the coming hours of the requested  city. */
    fetchCityForeCast(cityToForecast) {
        return new Promise((resolve, reject) => {

            this.$http({
                method: 'GET',
                url: 'http://api.openweathermap.org/data/2.5/forecast?q=' + cityToForecast + '&appid=9ae7cc8fabc82556be550290ccb8f1e2&units=metric'
            }).then((response) => {

                let forecastModel = new ForecastModel(
                    cityToForecast,
                    response.data.city.id,
                    this.getColorFromProperties(response.data.city.id));

                 /* Loop trough every forecast from response. Will need to do more defensive checking in real life. */
                response.data.list.forEach((forecast) => {
                    forecastModel.addForecast(
                        forecast.main.temp,
                        forecast.weather[0].icon,
                        forecast.wind.deg,
                        forecast.wind.speed,
                        new Date(forecast.dt * 1000)
                    );
                });
                resolve(forecastModel);
            }, function errorCallback(response) {
                reject(response);
            });
        });

    }

    makeCommaSeperatedCitiesId() {
        return this.citiesToFetch.map((cityToFetch) => cityToFetch.id).join();
    }

    getColorFromProperties(idToFind) {
        return this.citiesToFetch.find((myCity) => myCity.id == idToFind).displayColor;
    }
}

WeatherApiService.$inject = ['$http'];