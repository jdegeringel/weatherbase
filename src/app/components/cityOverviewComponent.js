'use strict';

/**
 * City overview component. Generates a overview of weather information based on a list of CityWeatherModels.
 * 
 * Missing good tests for this component, no time left sorry ;). 
 */
export const cityOverviewComponent = {
	templateUrl: 'app/components/cityOverviewComponentTemplate.html',
	bindings: {
		city: '=',
	},
	controllerAs: '$ctrl',
	controller:

		class CityOverviewController {
			constructor(ngNotify, $location) {

				this.ngNotify = ngNotify;
				this.$location = $location;
				/* 
					See https://openweathermap.org/weather-conditions for contract.
					Weather icon should be seperate component, shared between overview and forecast.
				*/
				this.iconMap = new Map([
					['01d', 'wi-day-sunny'],
					['02d', 'wi-day-cloudy'],
					['03d', 'wi-cloudy'],
					['04d', 'wi-cloudy'],
					['09d', 'wi-rain'],
					['10d', 'wi-sprinkle'],
					['11d', 'wi-day-thunderstorm'],
					['13d', 'wi-snow'],
					['50d', 'wi-fog'],
				]);
			}

			notImplementedAlert() {
				this.ngNotify.set("Oh dear, It seems I didn't have enough time to implement this, you can always f5 :)", {
					position: 'bottom',
					type: 'warn'
				});

			}

			getWeatherIcon(iconCode) {
				// for now replace night icons with day (01n becomes 01d), covers all icons but still a 'n/a' fallback
				let icon;
				if (iconCode) {
					icon = this.iconMap.get(iconCode.replace('n', 'd'));
				}
				return icon ? icon : 'wi-na';
			}

			// injection here
			static get $inject() {
				return ['ngNotify', '$location'];
			}
		}
};