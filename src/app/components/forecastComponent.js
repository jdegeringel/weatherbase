'use strict';

/**
 * Forecast component. Generates a overview of forecasts based on a ForecastModel.
 * 
 * Missing good tests for this component, no time left sorry ;). 
 */
import ApplicationProperties from './../ApplicationProperties';

export const forecastComponent = {
	templateUrl: 'app/components/forecastComponentTemplate.html',
	bindings: {
		forecast: '='

	},
	controllerAs: '$ctrl',
	controller:

		class ForecastController {

			constructor($location) {

				this.$location = $location;
				this.limit = 5; // default amount of forecasts to shown
				this.cityOverview = ApplicationProperties.getMyCities();

				/* 
					See https://openweathermap.org/weather-conditions for contract.
					Weather icon should be seperate component, shared between overview and forecast.
				*/
				this.iconMap = new Map([
					['01d', 'wi-day-sunny'],
					['02d', 'wi-day-cloudy'],
					['03d', 'wi-cloudy'],
					['04d', 'wi-cloudy'],
					['09d', 'wi-rain'],
					['10d', 'wi-sprinkle'],
					['11d', 'wi-day-thunderstorm'],
					['13d', 'wi-snow'],
					['50d', 'wi-fog'],
				]);
			}


			getWeatherIcon(iconCode) {
				// for now replace night icons with day (01n becomes 01d), covers all icons but still a 'n/a' fallback
				let icon;
				if (iconCode) {
					icon = this.iconMap.get(iconCode.replace('n', 'd'));
				}
				return icon ? icon : 'wi-na';
			}

			static get $inject() {
				return ['$location'];
			}
		}
};