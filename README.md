[TOC]

# WEATHERBASE #

An ES6 Angular 1.5 app to show weather information and forecast of some cities. 

[How a look at the result](http://polyprog.io/weatherbase)

This README describes features and design considerations of the app.

## How do I set it up ##

To set it up locally.


```
#!shell
bower install
npm install

# builds and starts the app localhost:4242
gulp

# builds the app
gulp build

# runs the unit test
gulp test


```

## App Features ##
Have a look: [http://polyprog.io/weatherbase](http://polyprog.io/weatherbase)

* An overview of the current weather of cities fetched from the openweathermap api.
* Showing a situation icon, temperature, wind speed, wind direction.
* Click on the cities to show a forecast for the coming hours with (not quite) infinite scrolling... or actually infinite clicking... can be improved.
* Easy to change or add cities via applicationProperties.
* Fully responsive.

Thinks NOT in the app but should be (given time)

* Not accessible

## Technical Features ##

* Modular application (most classes and tests have no angular context, just plain ES6)
* Transpiling with babel
* Gulp tasks to jshint, run test and build the app
* SCSS 
* Beautiful code ;)
* Tests, ofcourse

Thinks NOT in the app but should be (given time):

* Missing good tests for the 2 classes in components 
* Missing protractor like end-to-end tests 





## Design and Architecture ##

I challenged myself to do as little angular and as much as standard ES6 as possible for this project. In general it really paid of and I'm happy with the result. 

Most of the classes and especially, especially the tests are a big improvement over plain ES5 and Angular. No need to setup angular context anymore with it's proprietary dependency injection in unit tests.

I believe in the John Pappa angular style guide for coding convention and find  [this ES6 fork](https://github.com/rwwagner90/angular-styleguide-es6) really usefull for writing nice, portable code.

Ofcourse there are some considerations with this ES6 approach. Ofcourse you need a babel transpiler and karma needs it's own preprocessor and many more dependencies, just have a look at the package.json and gulp file. The code is more simple but there's some trade off with a more complicated build setup.

## Way of Working ##

Even for this small project I like to have some flow. Kanbanflow is really usefull for me!

![Screenshot from 2017-06-25 12-43-04.png](https://bitbucket.org/repo/Ag6bKkG/images/342315682-Screenshot%20from%202017-06-25%2012-43-04.png)