var gulp = require('gulp');
var babelify = require('babelify');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var browserify = require('browserify');
var merge = require('merge-stream');
var vinylSourceStream = require('vinyl-source-stream');
var vinylBuffer = require('vinyl-buffer');
var Server = require('karma').Server;

// Load all gulp plugins into the plugins object.
var plugins = require('gulp-load-plugins')();

var src = {
    html: 'src/**/*.html',
    font: 'src/font/**',
    libs: 'src/lib/**',
    scripts: {
        all: 'src/app/**/*.js',
        app: 'src/app/main.js'
    }
};

var build = 'build/';

var out = {
    libs: build + 'lib/',
    font: build + 'font/',
    scripts: {
        file: 'app.min.js',
        folder: build + './'
    }
}

gulp.task('html', function () {
    return gulp.src(src.html)
        .pipe(gulp.dest(build))
        .pipe(plugins.connect.reload());
});

/* The jshint task runs jshint with ES6 support. */
gulp.task('jshint', function () {
    return gulp.src(src.scripts.all)
        .pipe(plugins.jshint({
            esnext: true // Enable ES6 support
        }))
        .pipe(plugins.jshint.reporter('jshint-stylish'));
});

gulp.task('styles', function () {
    var sassStream;
    var cssStream;

    sassStream = gulp.src('./src/css/main.scss')
        .pipe(sass({
            loadPath: [
                './src',

            ]
        }));
    cssStream = gulp.src('./src/css/*.css');

    return merge(sassStream, cssStream)
        .pipe(concat('main.css'))
        .pipe(gulp.dest(build + '/css'));
});


gulp.task('libs', function () {
    /* In a real project you of course would use npm or bower to manage libraries. */
    return gulp.src(src.libs)
        .pipe(gulp.dest(out.libs))
        .pipe(plugins.connect.reload());
});


gulp.task('font', function () {
    /* In a real project you of course would use npm or bower to manage libraries. */
    return gulp.src(src.font)
        .pipe(gulp.dest(out.font))
        .pipe(plugins.connect.reload());
});

/* Compile all script files into one output minified JS file. */
gulp.task('scripts', ['jshint'], function () {

    var sources = browserify({
            entries: src.scripts.app,
            debug: true // Build source maps
        })
        .transform(babelify.configure({
            // You can configure babel here!
            // https://babeljs.io/docs/usage/options/
            presets: ["es2015"]
        }));

    return sources.bundle()
        .pipe(vinylSourceStream(out.scripts.file))
        .pipe(vinylBuffer())
        .pipe(plugins.sourcemaps.init({
            loadMaps: true // Load the sourcemaps browserify already generated
        }))
        .pipe(plugins.ngAnnotate())
        .pipe(plugins.uglify())
        .pipe(plugins.sourcemaps.write('./', {
            includeContent: true
        }))
        .pipe(gulp.dest(out.scripts.folder))
        .pipe(plugins.connect.reload());

});

gulp.task('serve', ['build', 'watch'], function () {
    plugins.connect.server({
        root: './build',
        port: 4242,
        livereload: true
    });
});

gulp.task('watch', function () {
    gulp.watch(src.libs, ['libs']);
    gulp.watch(src.html, ['html']);
    gulp.watch(src.scripts.all, ['scripts']);
});

/**
 * Run test once and exit
 */
gulp.task('test', function (done) {
    new Server({
        configFile: __dirname + '/karma.conf.js',
        singleRun: true
    }, done).start();
});

gulp.task('build', ['styles', 'font', 'scripts', 'html', 'libs']);
gulp.task('default', ['serve']);
